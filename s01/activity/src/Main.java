import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String firstName;
        String lastName;
        Double firstSubject;
        Double secondSubject;
        Double thirdSubject;

        Scanner userInput = new Scanner(System.in);

        System.out.println("First Name:");
        firstName = userInput.nextLine();

        System.out.println("Last Name:");
        lastName = userInput.nextLine();

        System.out.println("First Subject Grade:");
        firstSubject = userInput.nextDouble();

        System.out.println("Second Subject Grade:");
        secondSubject = userInput.nextDouble();

        System.out.println("Third Subject Grade:");
        thirdSubject = userInput.nextDouble();

        System.out.println("Good day, " + firstName + " " + lastName + ". " + "Your grade average is: " + (firstSubject + secondSubject + thirdSubject) / 3);
    }
}