package com.zuitt.example;

public class Variables {
    public static void main(String[] args){
        // variable declaration - include data type
        int age;
        char middle_name;

        // variable declaration vs initialization
        int x;
        int y = 0;

        System.out.println("The value of y is " + y);
    }
}
