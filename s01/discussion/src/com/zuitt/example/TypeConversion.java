package com.zuitt.example;

import java.util.Scanner;

public class TypeConversion {
    public static void main(String[] args){
        Scanner userInput = new Scanner(System.in);
        System.out.println("How old are you? ");

        // If we need to convert it in to a numerical data type such as Double, we can pass in the Scanner input as an argument to the Double class constructor so:
        double age = new Double(userInput.nextLine());
        System.out.println("This is a confirmation that you are " + age + " years old.");
    }
}
