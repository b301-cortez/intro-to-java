package com.zuitt.example;

public class HelloWorld {
    // in Java every standalone program must have a "main method" as the starting point for the execution of the program.
    /*
    * public -> it is an access specifier that indicates a method is accessible from anywhere in the program.
    *
    * static -> it means that the method belongs to the class itself rather than instance of the class. The method needs to be static because it is called by Java runtime system before any objects are created.
    *
    * void -> it is the return type if the method indicating that the main method does not return anything.
    *
    * main -> it is the name of the method. The java runtime system looks for this exact method to start the program.
    *
    * (String[] args) -> a parameter severs as a way for java program to receive information or instructions from the outside when it starts running
    * */

    public static void main(String[] args) {
        System.out.println("Hello world!");
    }
}
