package com.zuitt.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Driver newDriver = new Driver();
//        Scanner input = new Scanner();

        newDriver.setName("Alech");
//        System.out.println(newDriver.name); // cannot be acessed because name is private
        System.out.println(newDriver.getName());

        Driver newDriver2 = new Driver("Elly");
        System.out.println(newDriver2.getName());

        // -----------------

        Car myCar = new Car();
        System.out.println("This car is driven by " + myCar.getDriver());

        Car myCar2 = new Car("Model Y", "Tesla", 2020);
        System.out.println(myCar2.getCarName());
        System.out.println(myCar2.getBrand());
        System.out.println(myCar2.getYearOfMake());
        System.out.println(myCar2.getDriver());
    }
}
