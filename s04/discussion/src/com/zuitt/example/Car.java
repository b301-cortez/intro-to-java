package com.zuitt.example;

public class Car {

    // make Driver a component of a Car or one of the properties of Car

    private Driver d;
    private String name;
    private String brand;
    private int yearofMake;

    // constructors
    public Car(){
        this.d = new Driver("Alejandro");
        // whenever we create a new car, it will have a driver named Alejandro.
    }

    public Car(String name, String brand, int yearofMake){
        this.name = name; // "within this class, access property name, and so on is the purpose of "this"".
        this.brand = brand;
        this.yearofMake = yearofMake;
        this.d = new Driver("Alejandro");
    }

    // setters
    public void setCarName(String name){
        this.name = name;
    }

    public void setBrand(String brand){
        this.brand = brand;
    }

    public void setYearOfMake(int yearOfMake){
        this.yearofMake = yearOfMake;
    }


    // getters
    public String getCarName(){
        return name;
    }

    public String getBrand(){
        return brand;
    }

    public int getYearOfMake(){
        return yearofMake;
    }

    public String getDriver(){
        return this.d.getName();
        // Car.Driver.getName()
    }



}
