package com.zuitt.example;

public class Driver {
    private String name; // cannot be accessed because its private

    // setter -> set the value
    // "void" -> used with no return statement
    public void setName(String newName){
        this.name = newName;
    }

    // getter -> to access property name
    public String getName(){
        return this.name;
    }

    public Driver(){}

    public Driver (String name){
        this.name = name;
    }


}
