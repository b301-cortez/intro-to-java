public class Main {
    public static void main(String[] args) {

        // for loops -> works only if the condition is true.
        // i = 0 -> initial or starting value
        // i < 10 -> condition to be satisfied
        // i++ -> change of value
        for (int i = 0; i < 10; i++) {
            System.out.println("Current count: " + i);
        }

        // array for loop
        int[] hundreds = {100, 200, 300, 400, 500};
        for (int i = 0; i < hundreds.length; i++){
            // i = 0; true; hundreds[0]=100 -> i++ -> 1
            // i = 1; true; hundreds[1]=200 -> i++ -> 2
            // i = 2; true; hundreds[2]=300 -> i++ -> 3
            // i = 3; true; hundreds[3]=400 -> i++ -> 4
            // i = 4; true; hundreds[4]=500 -> i++ -> 5
            // i = 5; false; for loop stops
            System.out.println(hundreds[i]);
        }

        System.out.println("Other approach -------");
        for(int hundred: hundreds){
            System.out.println(hundred);
        }


        //while loop
        // moves after knowing the condition
        int i = 8;

        while (i < 10){
            System.out.println("Current count: " + i);
            i++;
        }


        // do-while loop -> runs at least once
        // runs the "do" before knowing the condition in the "while".
        // moves before knowing the condition
        int y = 0;
        do {
            System.out.println("y value: " + y);
        } while(y != 0);

    }
}