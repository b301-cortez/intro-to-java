import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        // "Scanner" -> is a class
        // "input" -> is an object with the name "input"
        // "new" ->
        int num = 0;

        System.out.println("Input a number");
        try {
            num = input.nextInt();
        } catch(Exception e){ // "e" is a variable representing an exception object
            System.out.println("Invalid input, this program only accepts integer inputs."); // "try and catch" is an error handler and will proceed to the next line of code if there is an error.
            // e.printStackTrace(); // a way to show information about an error that occurred in Java
        }
        System.out.println("The number you entered is : " + num); // this code and the codes below, will run because of try and catch
        System.out.println("Next line of codes.");
    }
}
