import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int num;

        System.out.println("Input an integer whose factorial will be computed:");

        Scanner input = new Scanner(System.in);

        // while loop, for loop and exception handling
        try {
            num = input.nextInt();

            if (num < 0){
                System.out.println("Cannot compute for factorials of negative numbers.");
            } else {
                int answer = 1;

                // while loop
//                int counter = 1;
//
//                while (counter <= num){
//                    answer *= counter;
//                    counter++;
//                }

                // for loop
                for (int counter = 1; counter <= num; counter++){
                    answer *= counter;
                }

                System.out.println("The factorial of " + num + " is " + answer);
            }

        }catch(Exception e){
            System.out.println("Invalid input, this program only accepts integer inputs.");
            e.printStackTrace();
        }

    }
}