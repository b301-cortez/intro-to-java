package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Collections {
    public static void main(String[] args){

        // Arrays are fixed-size data structure that stores collection of elements of the same type.

        int[] intArray = new int[5]; // "5" is the size of the array
        System.out.println("Initial state of intArray: ");
        System.out.println(Arrays.toString(intArray));


        // index starts with 0
        // [0, 0, 0, 0, 0]
        intArray[0] = 1;
        intArray[intArray.length - 1] = 5;
        System.out.println("Updated Array: ");
        System.out.println(Arrays.toString(intArray));

        // intArray[intArray.length] = 6; not possible with array or non-resizable arrays because it has fixed length.


        // Multidimensional Arrays -> non-resizable
        // it includes rows and cols
        String[][] classroom = new String[3][3]; // 3rows, 3cols

        // First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";

        // Second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "Junjun";
        classroom[1][2] = "Jobert";

        // third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        System.out.println("Multidimensional Array: ");
        System.out.println(Arrays.deepToString(classroom));



        // ArrayList -> resizable
        // Dynamic data structures that can grow or shrink in size as needed
        System.out.println("------ArrayList------");

        ArrayList<String> students = new ArrayList<String>();
        System.out.println(students);

        // adding/inserting item/element
        students.add("John");
        students.add("Paul");
        System.out.println(students.size()); // to get the size or length of the arraylist
        System.out.println(students.get(0)); // to get an arraylist item or element using its index number
        System.out.println(students);

        // updating item/element
        students.set(1, "George");
        System.out.println(students);

        // removing item/element
        students.remove(1);
        System.out.println(students);

        // to empty arraylist
        students.clear();
        System.out.println(students);



        // HashMaps -> key:value pair data structures that provide a way to store and retrieve elements based on unique keys
        System.out.println("------HashMaps------");

        HashMap<String, String> job_position = new HashMap<String, String>();

        // adding
        // "Brandon" = key and "student" = value
        job_position.put("Brandon", "Student");
        job_position.put("Alice", "Dreamer");
        System.out.println(job_position);

        // to get or retrieve the value using key
        System.out.println(job_position.get("Alice"));

        // to remove a value using key
//        job_position.remove("Brandon");
//        System.out.println(job_position);

        System.out.println(job_position.keySet()); // to display the keys
        System.out.println(job_position.values()); // to display the values

        // updating a value using key
        job_position.put("Brandon", "Tambay");
        System.out.println(job_position);

        // updating a value using .replace method
        job_position.replace("Alice", "CEO");
        System.out.println(job_position);

    }
}
