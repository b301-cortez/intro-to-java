package com.zuitt.example;

public class ControlStructure {
    public static void main(String[] args) {
        // If Statements
        int num = 10;

        if(num > 0){
            System.out.println("The number is positive.");
        }else if(num < 0){
            System.out.println("The number is negative.");
        }else{
            System.out.println("The number is zero (not positive nor negative.");
        }


        //Switch Cases
        int directionValue = 4;
        switch(directionValue){
            case 1 :
                System.out.println("North");
                break;
            case 2 :
                System.out.println("south");
                break;
            case 3 :
                System.out.println("east");
                break;
            case 4 :
                System.out.println("west");
                break;
            default :
                System.out.println("Invalid");
        }
    }
}
