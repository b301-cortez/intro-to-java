package com.zuitt;

import java.util.ArrayList;
import java.util.HashMap;

public class WDC043_S2_A2 {
    public static void main(String[] args) {

        // Array
        int[] primeNumbers = new int[5];

        primeNumbers[0] = 2;
        primeNumbers[1] = 3;
        primeNumbers[2] = 5;
        primeNumbers[3] = 7;
        primeNumbers[primeNumbers.length - 1] = 11;

        System.out.println("The first prime number is: " + primeNumbers[0]);


        // Array List
        ArrayList<String> friends = new ArrayList<String>();

        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");

        System.out.println("My friends are: " + friends);


        // HashMap
        HashMap<String, Integer> currentInventory = new HashMap<String, Integer>();

        currentInventory.put("toothpaste", 15);
        currentInventory.put("toothbrush", 20);
        currentInventory.put("soap", 12);

        System.out.println("Our current inventory consists of: " + currentInventory);

    }
}
