package com.zuitt;

import java.util.Scanner;

public class WDC043_S2_A1 {
    public static void main(String[] args) {

        Scanner userInput = new Scanner(System.in);

        System.out.println("Input year to be checked if a leap year.");

        int leapYear = userInput.nextInt();

        if(leapYear % 4 == 0){
            System.out.println(leapYear + " is a leap year.");
        }else{
            System.out.println(leapYear + " is NOT a leap year.");
        }
    }
}
